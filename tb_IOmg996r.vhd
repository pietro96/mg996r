----------------------------------------------
-- Design Name: Test Bench for IO Manager for MG996R Servo - FPGA Interface
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_IOmg996r is
end tb_IOmg996r;
    
    architecture tb of tb_IOmg996r is
    
        component IOmg996r
            port (clk          : in std_logic;
                  rst          : in std_logic;
                  rx           : in std_logic;
                  servo_go     : in std_logic; 
                  pwm          : out std_logic;
                  bad_position : out std_logic;
                  servo_ready  : out std_logic;
                  tx           : out std_logic);
        end component;


        component uartTX is
            port(
              clk             : in std_logic;
              rst             : in std_logic; -- active low
              data2send       : in std_logic_vector(7 downto 0);
              send            : in std_logic;
              tx              : out std_logic;
              done            : out std_logic
            );
        end component;
    
        signal clk          : std_logic;
        signal rst          : std_logic;
        signal rx           : std_logic;
        signal servo_go     : std_logic;
        signal pwm          : std_logic;
        signal bad_position : std_logic;
        signal servo_ready  : std_logic;
        signal tx           : std_logic;
    
        -- define stimulator signal
        signal stimulator_data2send : std_logic_vector(7 downto 0);
        signal stimulator_send : std_logic;
        signal stimulator_done : std_logic;
        signal stimulator_tx : std_logic;

        constant TbPeriod : time := 20 ns;
        signal TbClock : std_logic := '0';
        signal TbSimEnded : std_logic := '0';
    
    begin
    
        dut : IOmg996r
        port map (clk          => clk,
                  rst          => rst,
                  rx           => stimulator_tx,
                  servo_go     => servo_go,
                  pwm          => pwm,
                  bad_position => bad_position,
                  servo_ready  => servo_ready,
                  tx           => tx);

        stimulator : uartTX
        port map (
            clk           => clk,       
            rst           => rst,        
            data2send     => stimulator_data2send,
            send          => stimulator_send,
            tx            => stimulator_tx,     
            done          => stimulator_done
        );
    
        -- Clock generation
        TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    
        -- EDIT: Check that clk is really your main clock signal
        clk <= TbClock;
    
        stimuli : process
        begin
            -- EDIT Adapt initialization as needed
            rx <= '0';
    
            -- Reset generation
            -- EDIT: Check that rst is really your reset signal
            
            rst <= '0';
            wait for 1*TbPeriod;
            rst <= '1';
            servo_go <= '1';
            stimulator_send       <= '1';

            --  00000001    11010100  11000000

            stimulator_data2send  <= "11000000" ;
            wait until stimulator_done ='1';
            stimulator_data2send  <= "11010100" ;
            wait until stimulator_done ='1';
            stimulator_data2send  <= "00000001" ;
            wait until stimulator_done ='1';
            stimulator_send       <= '0';

            wait until servo_ready = '1';

            -- EDIT Add stimuli here
            wait for 5 * TbPeriod;
    
            wait until servo_ready = '1';

            wait for 5 * TbPeriod;
            -- Stop the clock and hence terminate the simulation
            TbSimEnded <= '1';
            wait;
        end process;
    
    end tb;
    
    -- Configuration block below is required by some simulators. Usually no need to edit.
    
    configuration cfg_tb_IOmg996r of tb_IOmg996r is
        for tb
        end for;
    end cfg_tb_IOmg996r;
    