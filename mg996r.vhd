----------------------------------------------
-- Design Name: MG996R Servo - FPGA Interface
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mg996r is
    generic(
        position_precision      : integer := 24; -- bit numbers. This value should set in accordance with max()
        pwm_period              : integer := 175000;
        max_high                : integer := 120000;
        min_high                : integer := 35000;
        home                    : integer := 35000; -- should be a value between min_high and max_high
        actuating_time          : integer := 750000
    );
    port(
        clk             : in std_logic;
        rst             : in std_logic; -- active low
        set_position    : in std_logic_vector(position_precision-1 downto 0);
        go              : in std_logic;
        newDataRec      : in std_logic; -- if HIGH: data received from serial interface!
        sendNewData     : out std_logic;
        ready           : out std_logic;
        bad_position    : out std_logic;
        pwm             : out std_logic
    );
end mg996r;

architecture mg996r_architecture of mg996r is

begin -- begin architecture

    process(clk,rst)
    -- Define system state
    type state_type is (starting,idle, gen_high, gen_low, at_rst, validating, bad_state);
    variable state: state_type := starting;
    variable next_state : state_type := starting;
    -- Define constant 
    constant maxCnt : integer := pwm_period + actuating_time;
    -- Define counters
    variable current_position : integer range min_high to max_high; -- the position of the servo motor is encoded by the duty cyvle of pwm signal
    variable system_cnt : integer range 0 to maxCnt;
    -- Define bool 
    variable isPosSafe : boolean; 

    begin -- begin process
        if(rst='0') then
            state := at_rst;
            next_state := starting;
            current_position := home;
            pwm <= '0';
            sendNewData <= '0';
        elsif(rising_edge(clk)) then
            state := next_state;
            case(state) is
            
                when starting =>
                    current_position := home;
                    next_state := gen_high;
                    ready <= '0';
                    bad_position <= '0';
                    system_cnt := 0;
                    sendNewData <= '1';

                when gen_high =>
                    if(system_cnt = current_position) then 
                        pwm <= '0';
                        next_state := gen_low;
                    else
                        pwm <= '1';
                    end if;
                    
                    system_cnt := system_cnt + 1;
                    

                when gen_low =>
                    if(system_cnt = maxCnt) then
                        ready <= '1';
                        next_state := idle;
                    else
                        system_cnt := system_cnt + 1;
                    end if;

                when idle =>
                    system_cnt := 0;
                    sendNewData <= '1';
                    --if(go ='1' and set_position /= std_logic_vector(to_unsigned(current_position, set_position'length)) ) then
                    if(go ='1' ) then
                        next_state := validating;
                        isPosSafe := (set_position <= std_logic_vector(to_unsigned(max_high , set_position'length))  ) and (set_position >= std_logic_vector(to_unsigned(min_high , set_position'length)) );
                        current_position := to_integer(unsigned(set_position));
                        ready <= '0';
                    end if;

                when validating =>
                        -- This is a security feature. A BAD position
                        -- may lead to current peak.
                        if(isPosSafe) then
                            next_state := gen_high;
                        else
                            next_state := bad_state;                            
                        end if;
                        

                when others =>
                    bad_position <= '1'; 
                    ready <= '0';
                    next_state := starting;
                    pwm <= '0';
                    sendNewData <= '0';
            
            end case ;

            if (state /= idle and state /=starting and newDataRec='1') then
                sendNewData <= '0';
            end if ;

        end if;

    end process;

end mg996r_architecture ; -- mg996r_architecture