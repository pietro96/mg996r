<h1>Color Tracking with servo MG996R, Jetson Nano & FPGA Cyclone IV Board</h1>

In this project, it was developed a system capable of the following actions:

- Locate a green rectangle and report its center coordinates and anomaly with respect to an arbitrary origin. 
This task is done thanks to an OpenCV application written in C++ (see cpp folder). The application runs
on Jetson Nano board.
- The rotation of the green rectangle is replicated by servo motor controlled by and FPGA Board (A-C4E6E10).
  The Jetson Nano communicates the position of the green rectangle to the FPGA Board through serial communication.



<h2>Demo</h2>
[Watch the Video on YouTube](https://www.youtube.com/watch?v=l34daSNVviE)


[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/l34daSNVviE/0.jpg)](https://www.youtube.com/watch?v=l34daSNVviE)



