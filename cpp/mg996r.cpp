// @ P.Pennestri
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cmath>
#include <math.h>   
#include <gst/gst.h>

#include <fstream>
#include <iomanip>
#include <cstring>

#define PI 3.14159265
using namespace cv;
using namespace std;

static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
	double dx1 = pt1.x - pt0.x;
	double dy1 = pt1.y - pt0.y;
	double dx2 = pt2.x - pt0.x;
	double dy2 = pt2.y - pt0.y;
	return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}


int setLabel(cv::Mat& im, cv::Mat &im2, const std::string label, std::vector<cv::Point>& contour, double h, double w)
{
	int fontface = cv::FONT_HERSHEY_SIMPLEX;
	double scale = 5;
	int thickness = 3;
	int baseline = 0;

	cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
	cv::Rect r = cv::boundingRect(contour);

	cv::putText(im, label,Point(r.x+r.width, r.y) , fontface, scale, CV_RGB(255,0,0), thickness, 8);
	

	cv::circle(im, Point(r.x, r.y), 20, Scalar(255,0,0), 13);
	cv::circle(im, Point(r.x+r.width, r.y), 20, Scalar(255,0,0), 13);
	cv::circle(im, Point(r.x, r.y + r.height), 20, Scalar(255,0,0), 13);
	cv::circle(im, Point(r.x+r.width, r.y + r.height), 20, Scalar(255,0,0), 13);
	cv::circle(im, Point(r.x+0.5*r.width ,r.y +0.5*r.height ) ,20 , Scalar(0,0,255),13);

	cv::line(im,Point(0.5*w,0.8*h),Point(r.x+0.5*r.width ,r.y +0.5*r.height),(255,0,0),5);
	// compute the center
	cv::putText(im2,to_string(r.x+0.5*r.width ),Point(70, 90) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 2, 1);
	cv::putText(im2,to_string(r.y +0.5*r.height) ,Point(70, 130) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 2, 1);

	// compute the angle
	double Refangle = -atan2(r.y +0.5*r.height - 0.8*h , r.x+0.5*r.width - 0.5*w) * 180 / PI;
	cv::putText(im2,to_string(Refangle),Point(130, 170) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 2, 1);
	return (int) Refangle;
}

// This function read a given UART Port 
int readLastSerial(char *serialPortFilename)
{
    char readBuffer[1];
    int numBytesRead;

    FILE *serPort = fopen(serialPortFilename, "r");

	if (serPort == NULL)
	{
		return -1;
	}
		memset(readBuffer, 0, 1);
		fread(readBuffer, sizeof(char),1,serPort);
		/*if(sizeof(readBuffer) != 0)
		{
			printf(readBuffer);
		}*/

		if (readBuffer[0] == 0x01)
		{
			return 1;
		}else{
			return 0;
		}
	return -1;
}


int encodeCharAsHex (char *hexChar , int i){
	if(hexChar[i] == '0'){
		return 0x0;
	}else if (hexChar[i] == '1') {
		return 0x1;
	}else if (hexChar[i] =='2'){
		return 0x2;
	}else if (hexChar[i] =='3'){
		return 0x3;
	}else if (hexChar[i] =='4'){
		return 0x4;
	}else if (hexChar[i] =='5'){
		return 0x5;
	}else if (hexChar[i] =='6'){
		return 0x6;
	}else if (hexChar[i] =='7'){
		return 0x7;
	}else if (hexChar[i] =='8'){
		return 0x8;
	}else if (hexChar[i] =='9'){
		return 0x9;
	}else if (hexChar[i] =='a' or hexChar[i] =='A'){
		return 0xA;
	}else if (hexChar[i] =='b' or hexChar[i] =='B'){
		return 0xB;
	}else if (hexChar[i] =='c' or hexChar[i] =='C'){
		return 0xC;
	}else if (hexChar[i] =='d' or hexChar[i] =='D'){
		return 0xD;
	}else if (hexChar[i] =='e' or hexChar[i] =='E'){
		return 0xE;
	}else if (hexChar[i] =='f' or hexChar[i] =='F'){
		return 0xF;
	}else{
		return -1;
	}

}


void setAngle(int angle , char *serialPortFilename)
{
	int min_high = 35000;
    int max_high = 120000;
    int pulseHigh  = min_high + (max_high - min_high)/(180) * angle;
    
    // define packs of bits to send
    int pack1_1, pack1_2,pack2_1, pack2_2,pack3_1, pack3_2;
    
    // define stream
    std::stringstream hex_stream;
    hex_stream << std::hex << std::setw(6) << std::setfill('0') << pulseHigh ;
    std::string hex_pulseHigh_string = hex_stream.str();
    // convert string to char
    char *hex_pulseHigh = new char[hex_pulseHigh_string.length() + 1];
	strcpy(hex_pulseHigh, hex_pulseHigh_string.c_str());

	pack1_1 = encodeCharAsHex(hex_pulseHigh,5);
	pack1_2 = encodeCharAsHex(hex_pulseHigh,4);
	
	pack2_1 = encodeCharAsHex(hex_pulseHigh,3);
	pack2_2 = encodeCharAsHex(hex_pulseHigh,2);
	
	pack3_1 = encodeCharAsHex(hex_pulseHigh,1);
	pack3_2 = encodeCharAsHex(hex_pulseHigh,0); 
	int combination =(pack3_2 << 20) | (pack3_1 << 16) | (pack2_2 << 12)  | (pack2_1 << 8) | (pack1_2 << 4) | pack1_1; 
    
	//open com with FPGA
    std::ofstream fpga;
	fpga.open(serialPortFilename, std::ios::binary | std::ios::out);
    fpga.write((char*) &combination, 3); 
	fpga.close();
    
}



int main(int argc, char* argv[])
{

VideoCapture cap(0); //Open the default video camera
// Remove the following comment if you would like to use Gstramer
//cv::VideoCapture cap("v4l2src device=/dev/video0 ! videoscale ! videorate ! video/x-raw, width=1080, height=800, framerate=30/1 ! videoconvert ! appsink");

char serialPortFilename[] = "/dev/ttyUSB0";

// if not success, exit program
if (cap.isOpened() == false)
{
	cout << "Cannot open the video camera" << endl;
	cin.get(); //wait for any key press
	return -1;
}

double dWidth = cap.get(CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
double dHeight = cap.get(CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

cout << "Resolution of the video : " << dWidth << " x " << dHeight << endl;
cout<< "gstreamer_cap="<<CAP_GSTREAMER<<endl;

string window_name = "PCAMERA - ESL LAB";
namedWindow(window_name); //create a window called “My Camera Feed”

while (true)
{
	Mat frame, hsvFrame ,  binFrame;
	Mat resultHSV, grayOut ,binOut, outFrame1,outFrame2,outFrame3,outFrame4,outFrame5;
	Mat maskBGR, maskHSV;
	Mat hsvFrame_channels[3];
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	bool bSuccess = cap.read(frame); // read a new frame from video

	Size dsize;
	dsize.width = dWidth/2;
	dsize.height = dHeight/2;

	// Define color range to pick.
	/*cv::Vec3b bgrPixel(100, 100, 100);
	int thresh = 100;
	cv::Scalar minBGR = cv::Scalar(0,  0, bgrPixel.val[2] - thresh) ;
	cv::Scalar maxBGR = cv::Scalar(70, 255, bgrPixel.val[2] + thresh) ; */

	//Breaking the while loop if the frames cannot be captured
	if (bSuccess == false){
		cout << "Video camera is disconnected" << endl;
		cin.get(); //Wait for any key press
		break;
	}

	// convert to HSV
	cv::cvtColor(frame, hsvFrame, cv::COLOR_BGR2HSV);
	
	// Pick the right range to filter with this tool https://alloyui.com/examples/color-picker/hsv.html
	//cv::Scalar minHSV = cv::Scalar(75, 50, 50); //night
	//cv::Scalar maxHSV = cv::Scalar(110, 255,255); // night 
	cv::Scalar minHSV = cv::Scalar(75, 100, 100); // day
	cv::Scalar maxHSV = cv::Scalar(95, 255,255); // day
	cv::inRange(hsvFrame, minHSV, maxHSV, maskHSV);
	cv::bitwise_and(hsvFrame, hsvFrame, resultHSV, maskHSV); 
	
	cv::split( resultHSV, hsvFrame_channels );
	cv::threshold(hsvFrame_channels[2], binFrame,100,255,cv::THRESH_BINARY);

	
  	//Mat infoFrame;
	Mat infoFrame(dsize ,CV_8UC3, cv::Scalar(255, 0, 0));
	cv::putText(infoFrame,"Info Box",Point(40, 50) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 3, 4);
	cv::putText(infoFrame,"x:",Point(40, 90) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 3, 1);
	cv::putText(infoFrame,"y:",Point(40, 130) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 3, 1);
	cv::putText(infoFrame,"Ang:",Point(40, 170) , cv::FONT_HERSHEY_SIMPLEX, 1, CV_RGB(255,255,255), 3, 1);
  	// contours
  	std::vector<cv::Point> approx;
	cv::Mat cntFrame = frame.clone();

  	findContours( binFrame, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );
  	for (int i = 0; i < contours.size(); i++){
  		cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);
  		// Remove small or non-convex objects 
		if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
			continue;


		if (approx.size() >= 4 && approx.size() <= 6)
		{
			// Number of vertices of polygonal curve
			int vtc = approx.size();

			// Get the cosines of all corners
			std::vector<double> cos;
			for (int j = 2; j < vtc+1; j++)
				cos.push_back(angle(approx[j%vtc], approx[j-2], approx[j-1]));

			// Sort ascending the cosine values
			std::sort(cos.begin(), cos.end());

			// Get the lowest and the highest cosine
			double mincos = cos.front();
			double maxcos = cos.back();

			// Use the degrees obtained above and the number of vertices
			// to determine the shape of the contour
			if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3){
				int angle = setLabel(cntFrame,infoFrame ,"RECT", contours[i],dHeight,dWidth);
				
				if(readLastSerial(serialPortFilename) == 1){
					setAngle(angle , serialPortFilename);
				}
				
				//std::cout<<readLastSerial(serialPortFilename);
				
			}
				//std::cout<<readLastSerial(serialPortFilename);
		
		}

  	}
  	cv::circle(cntFrame, Point(0.5*dWidth, 0.8*dHeight) ,20 , Scalar(0,255,0),13);
	// Conver to display result
	cv::cvtColor( resultHSV , resultHSV, cv::COLOR_HSV2BGR);
	cv::cvtColor(binFrame, binOut, cv::COLOR_GRAY2BGR);
	cv::cvtColor( hsvFrame_channels[2], grayOut, cv::COLOR_GRAY2BGR);

	//resize to display result	
	resize(frame, frame, dsize, 0.1, 0.1,INTER_LINEAR);
	resize(resultHSV, resultHSV, dsize, 0.1, 0.1,INTER_LINEAR);
	resize(binOut, binOut, dsize, 0.1, 0.1,INTER_LINEAR);
	resize(grayOut, grayOut, dsize, 0.1, 0.1,INTER_LINEAR);
	resize(infoFrame, infoFrame, dsize, 0.1, 0.1,INTER_LINEAR);
	resize(cntFrame, cntFrame, dsize, 0.1, 0.1,INTER_LINEAR);
	// concat to display final result
	hconcat(frame,resultHSV,outFrame1);
	hconcat(grayOut,binOut,outFrame2);
	hconcat(infoFrame,cntFrame,outFrame3);
	vconcat(outFrame1,outFrame2,outFrame4);
	vconcat(outFrame4,outFrame3,outFrame5);
	imshow(window_name, outFrame5);
 
	if (waitKey(10) == 27){
		cout << "Esc key is pressed by user. Stoppig the video" << endl;
		break;
	}

}

	return 0;

}
