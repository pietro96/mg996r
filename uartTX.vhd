----------------------------------------------
-- Design Name: UART TX
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uartTX is
  generic(
    pulse2cnt : integer := 5199 -- @ 50MHz 9600 br ==> (104 * 10^-6)/(20*10^-9) -1 
  );
  port(
    clk             : in std_logic;
    rst             : in std_logic; -- active low
    data2send       : in std_logic_vector(7 downto 0);
    send            : in std_logic;
    tx              : out std_logic;
    done            : out std_logic
  );
end uartTX;

architecture uartTXArc of uartTX  is
begin -- architecture
    process(clk,rst)
        -- define state
        type state_txpe is (at_rst, transmitting, open_ch, close_ch, idle);
        variable state : state_txpe := idle;
        variable next_state : state_txpe := idle;
        -- define cnts
        variable bitcnt : integer range 0 to 7:= 0;
        variable cnt : integer range 0 to pulse2cnt;
        -- local reg
        variable local_data2send : std_logic_vector(7 downto 0); 
    begin -- begin process
        if(rst='0') then
            done <= '0';
            cnt := 0;
            bitcnt := 0;
            next_state:= idle;
        elsif(rising_edge(clk)) then
            state := next_state;

            case(state) is
            
                when open_ch =>
                    tx <= '0';
                    if(cnt = pulse2cnt) then
                        next_state := transmitting;
                        cnt := 0;
                    else
                        cnt := cnt + 1;
                    end if;

                when transmitting =>
                    tx <= local_data2send(bitcnt);
                    if(cnt=pulse2cnt and bitcnt=7) then
                        cnt := 0;
                        next_state := close_ch;
                    elsif(cnt=pulse2cnt) then
                        cnt :=0;
                        bitcnt := bitcnt + 1;
                    else
                        cnt := cnt +1;
                    end if;
                    
                when close_ch =>
                    tx <= '1';
                    if(cnt = pulse2cnt) then
                        next_state := idle;
                        done <= '1';
                        cnt := 0;
                    else
                        cnt := cnt + 1;
                    end if;


                when idle =>
                    tx <= '1';
                    done <= '0';
                    if(send = '1') then
                        next_state := open_ch;
                        bitcnt := 0;
                        cnt := 0;
                        local_data2send := data2send;
                    end if;

                when others =>
                    done <='0';
                    tx <= '1';
                    next_state := idle;
            end case ;

        end if;

    end process;
end uartTXArc ; 