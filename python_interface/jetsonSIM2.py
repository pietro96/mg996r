import os
import serial 
from serial import Serial
import codecs
import time

def explode(word): 
    return [char for char in word]  

fpga_clk_period = 20 *1e-9 #20ns
pulses_in_period = 2500

def deg2pulse(angle):
    min_high = 35000
    max_high = 120000
    return min_high + (max_high - min_high)/(180) * angle


#ser = serial.Serial('/dev/ttyUSB0', 9600)
ser = serial.Serial('COM3', 9600)
isForward = True
angle = 0
try:
    while(True):

            for i in range(0, 181):
                print(isForward)
                if(isForward):
                    angle = i
                else:
                    angle = 180 -i
                #angle = i
                print('Selected angle:',angle, 'degrees')
                pulses_high = str(hex(int(deg2pulse(angle)))).replace('0x', '')
        
                while(len(pulses_high)<6):
                    pulses_high = '0'+pulses_high
                
                print(pulses_high)
                pulses_high = explode(pulses_high)
        
                #cmd = 'echo -e -n "'+chr(92)+'x'+pulses_high[2]+pulses_high[3]+chr(92)+'x'+pulses_high[0]+pulses_high[1]+'" > /dev/ttyUSB0'
                pack1=pulses_high[0]+pulses_high[1]
                pack2=pulses_high[2]+pulses_high[3]
                pack3=pulses_high[4]+pulses_high[5]
                
        
                while(True):
                    readFromSerial = ser.read()
                    if(readFromSerial ==  b'\x01'):
                        ser.write(codecs.decode(pack3, 'hex'))
                        ser.write(codecs.decode(pack2, 'hex'))
                        ser.write(codecs.decode(pack1, 'hex'))
                        #time.sleep(0.01)
                        break
            isForward = not isForward
                
except KeyboardInterrupt:
    ser.close()

    






#print("The hexadecimal form of 23 is" + hex(23)) 