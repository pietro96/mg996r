import os
import serial 
from serial import Serial
import codecs
import time

def explode(word): 
    return [char for char in word]  

fpga_clk_period = 20 *1e-9 #20ns
pulses_in_period = 2500

def deg2pulse(angle):
    min_high = 35000
    max_high = 120000
    return min_high + (max_high - min_high)/(180) * angle


ser = serial.Serial('/dev/ttyUSB0', 9600)
#ser = serial.Serial('COM3', 9600)
isForward = True
angle = 0
try:
    while(True):
	readFromSerial = ser.read()
	if(readFromSerial ==  b'\x01'):
		print('Can send')
	else:
		print('NO')
                
except KeyboardInterrupt:
    ser.close()
