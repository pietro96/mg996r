----------------------------------------------
-- Design Name: Test Bench for MG996R Servo - FPGA Interface
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_mg996r is
    generic(
        position_precision      : integer := 24
    );
end tb_mg996r;

architecture tb of tb_mg996r is

    component mg996r
        port (
            clk             : in std_logic;
            rst             : in std_logic; -- active low
            set_position    : in std_logic_vector(position_precision-1 downto 0);
            go              : in std_logic;
            newDataRec      : in std_logic; -- if HIGH: data received from serial interface!
            sendNewData     : out std_logic;
            ready           : out std_logic;
            bad_position    : out std_logic;
            pwm             : out std_logic
        );
    end component;

    signal clk          : std_logic;
    signal rst          : std_logic;
    signal set_position : std_logic_vector (position_precision-1 downto 0);
    signal go           : std_logic;
    signal newDataRec   : std_logic;
    signal sendNewData  : std_logic;
    signal ready        : std_logic;
    signal bad_position : std_logic;
    signal pwm          : std_logic;

    constant TbPeriod : time := 20 ns; 
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : mg996r
    port map (clk          => clk,
              rst          => rst,
              set_position => set_position,
              go           => go,
              newDataRec   => newDataRec,
              sendNewData  => sendNewData,
              ready        => ready,
              bad_position => bad_position,
              pwm          => pwm);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    clk <= TbClock;

    stimuli : process
    begin

        --set_position <= (others => '0');
        newDataRec <= '0';
        go <= '0';
        rst <= '0';
        wait for 1*TbPeriod;
        rst <= '1';
        go <= '1';
        wait until ready = '1';
        go <= '0';
        wait for 5*TbPeriod;
        go <= '1';
        newDataRec <= '1';
        set_position <= std_logic_vector(to_unsigned(120000, set_position'length));
        wait until ready = '1';
        wait for 5*TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

configuration cfg_tb_mg996r of tb_mg996r is
    for tb
    end for;
end cfg_tb_mg996r;