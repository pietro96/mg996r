onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mg996r/clk
add wave -noupdate /tb_mg996r/rst
add wave -noupdate /tb_mg996r/set_position
add wave -noupdate /tb_mg996r/go
add wave -noupdate /tb_mg996r/ready
add wave -noupdate /tb_mg996r/bad_position
add wave -noupdate /tb_mg996r/pwm
add wave -noupdate /tb_mg996r/TbClock
add wave -noupdate /tb_mg996r/TbSimEnded
add wave -noupdate -divider Signals
add wave -noupdate /tb_mg996r/dut/line__36/state
add wave -noupdate /tb_mg996r/dut/line__36/next_state
add wave -noupdate /tb_mg996r/dut/line__36/current_position
add wave -noupdate /tb_mg996r/dut/line__36/system_cnt
add wave -noupdate /tb_mg996r/dut/line__36/isPosSafe
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {18500159189 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 257
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {18500159049 ps} {18500160051 ps}
