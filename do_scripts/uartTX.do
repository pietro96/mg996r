onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_uarttx/clk
add wave -noupdate /tb_uarttx/rst
add wave -noupdate /tb_uarttx/data2send
add wave -noupdate /tb_uarttx/send
add wave -noupdate /tb_uarttx/tx
add wave -noupdate /tb_uarttx/done
add wave -noupdate /tb_uarttx/TbClock
add wave -noupdate /tb_uarttx/TbSimEnded
add wave -noupdate -divider State
add wave -noupdate /tb_uarttx/dut/line__21/state
add wave -noupdate /tb_uarttx/dut/line__21/next_state
add wave -noupdate /tb_uarttx/dut/line__21/bitcnt
add wave -noupdate /tb_uarttx/dut/line__21/cnt
add wave -noupdate /tb_uarttx/dut/line__21/local_data2send
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {152 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 194
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {7684096 ps}
