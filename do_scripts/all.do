onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_iomg996r/dut/servo/clk
add wave -noupdate /tb_iomg996r/dut/servo/rst
add wave -noupdate -divider {DUT: SERVO}
add wave -noupdate /tb_iomg996r/dut/servo/set_position
add wave -noupdate /tb_iomg996r/dut/servo/go
add wave -noupdate /tb_iomg996r/dut/servo/ready
add wave -noupdate /tb_iomg996r/dut/servo/bad_position
add wave -noupdate /tb_iomg996r/dut/servo/pwm
add wave -noupdate /tb_iomg996r/dut/servo/newDataRec
add wave -noupdate /tb_iomg996r/dut/servo/sendNewData
add wave -noupdate -divider {DUT: RX}
add wave -noupdate /tb_iomg996r/dut/reciver/rx
add wave -noupdate /tb_iomg996r/dut/reciver/recData
add wave -noupdate /tb_iomg996r/dut/reciver/newData
add wave -noupdate -divider {DUT: TX}
add wave -noupdate /tb_iomg996r/dut/transmitter/data2send
add wave -noupdate /tb_iomg996r/dut/transmitter/send
add wave -noupdate /tb_iomg996r/dut/transmitter/tx
add wave -noupdate /tb_iomg996r/dut/transmitter/done
add wave -noupdate -divider STIMULATOR
add wave -noupdate /tb_iomg996r/stimulator/data2send
add wave -noupdate /tb_iomg996r/stimulator/send
add wave -noupdate /tb_iomg996r/stimulator/tx
add wave -noupdate /tb_iomg996r/stimulator/done
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {21372536515 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 368
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {38850231 ns}
