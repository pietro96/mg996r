----------------------------------------------
-- Design Name: IO Manager for MG996R Servo - FPGA Interface
-- Author: Pietro Pennestri
-- mail: pietro.pennestri@gmail.com
-- website: https://pennestri.me
-----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity IOmg996r is
  generic(
    -- generic for uart
    pulse2cnt : integer := 5199 ;-- @ 50MHz 9600 br ==> (104 * 10^-6)/(20*10^-9) -1 
    half_pulse2cnt : integer := 2600; -- (pulse2cnt+1)/2
    -- generic for mg996r 
    position_precision      : integer := 24; -- bit numbers. This value should set in accordance with max()
    pwm_period              : integer := 175000;
    max_high                : integer := 120000;
    min_high                : integer := 35000;
    home                    : integer := 35000; -- should be a value between min_high and max_high
    actuating_time          : integer := 750000
  );
  port (
    clk          : in std_logic;
    rst          : in std_logic;
    rx           : in std_logic;
    servo_go     : in std_logic;         
    pwm          : out std_logic;
    bad_position : out std_logic;
    servo_ready  : out std_logic;
    tx           : out std_logic
  );
end IOmg996r ;

architecture arch of IOmg996r is

    --define components 
    component mg996r is
        generic(
            position_precision      : integer ; -- bit numbers. This value should set in accordance with max()
            pwm_period              : integer ;
            max_high                : integer ;
            min_high                : integer ;
            home                    : integer ; -- should be a value between min_high and max_high
            actuating_time          : integer 
        );
        port(
            clk             : in std_logic;
            rst             : in std_logic; -- active low
            set_position    : in std_logic_vector(position_precision-1 downto 0);
            go              : in std_logic;
            newDataRec      : in std_logic; -- if HIGH: data received from serial interface!
            sendNewData     : out std_logic;
            ready           : out std_logic;
            bad_position    : out std_logic;
            pwm             : out std_logic
        );
    end component;

    component uartRec is
        generic(
            pulse2cnt           : integer ;-- @ 50MHz 9600 br ==> (104 * 10^-6)/(20*10^-9) -1 
            half_pulse2cnt      : integer -- (pulse2cnt+1)/2
        );
        port(
            clk : in std_logic;
            rst : in std_logic; -- low active rst
            rx :  in std_logic;
            --- Out
            recData : out std_logic_vector(7 downto 0);
            newData : out std_logic
        );
    end component;

    component uartTX is
        generic(
          pulse2cnt       : integer 
        );
        port(
          clk             : in std_logic;
          rst             : in std_logic; -- active low
          data2send       : in std_logic_vector(7 downto 0);
          send            : in std_logic;
          tx              : out std_logic;
          done            : out std_logic
        );
    end component;

    -- signal for mg996r
    signal    set_position    : std_logic_vector(position_precision-1 downto 0);
    signal    ready           : std_logic;
    signal    newDataRec      : std_logic;
    signal    sendNewData     : std_logic;
    -- Signal for uart rec
    signal    recData : std_logic_vector(7 downto 0);
    signal    newData : std_logic;
    -- Signal for uart tx
    signal   tx_done    : std_logic;
    signal   data2send  : std_logic_vector(7 downto 0) ;

begin --begin architecture

    servo_ready <= ready;
    data2send <= "0000000"&sendNewData;

    servo : mg996r
    generic map (
        position_precision   =>  position_precision,
        pwm_period           =>  pwm_period,
        max_high             =>  max_high, 
        min_high             =>  min_high,
        home                 =>  home, 
        actuating_time       =>  actuating_time
    )
    port map (clk          => clk,
            rst            => rst,
            set_position   => set_position,
            go             => servo_go,
            newDataRec     => newDataRec,
            sendNewData    => sendNewData,
            ready          => ready,
            bad_position   => bad_position,
            pwm            => pwm
    );

    reciver: uartRec
    generic map(
        pulse2cnt          => pulse2cnt,
		half_pulse2cnt     => half_pulse2cnt
    )
    port map(
        clk            => clk,
		rst            => rst,
		rx             => rx,
		recData        => recData,
		newData        => newData
    );

    transmitter: uartTX
    generic map(
        pulse2cnt => pulse2cnt
    )
    port map(
        clk             => clk,
        rst             => rst,
        data2send       => data2send,
        send            => '1',
        tx              => tx,
        done            => tx_done
    );


    process(clk,rst)
        --type state_type is (1st8, 2nd8, 3rd8, wait2go ,send_go, at_rst, idle);
        type state_txpe is (first8, second8, third8, setPos, at_rst, idle);
        variable state : state_txpe := idle;
        variable next_state : state_txpe := idle;
        variable tmp_servoPosition : std_logic_vector(23 downto 0);
    begin -- begin process
        if (rst='0') then
            next_state := idle;
        elsif (rising_edge(clk)) then
            state := next_state;
            case(state) is
                when first8 =>
                    if(newData ='1') then
                        next_state := second8;
                    else
                        tmp_servoPosition(7 downto 0) := recData;
                    end if;
            
                when second8 => 
                    if(newData ='1') then
                        next_state := third8;
                    else
                        tmp_servoPosition(15 downto 8) := recData;
                    end if;
            
                when third8 =>
                    tmp_servoPosition(23 downto 16) := recData;
                    next_state := setPos;
                                   
                when setPos =>
                    set_position <= tmp_servoPosition;
                    next_state := idle;
                    newDataRec <= '1';
            
                when idle =>
                    if(newData ='1') then
                        next_state := first8;
                    end if;
                    newDataRec <= '0';
    
                when others =>
                    next_state := idle;
            end case;
            
        end if ;
    end process;
end architecture ; -- arch